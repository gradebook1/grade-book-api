import {
  BadRequestException,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { nanoid } from 'nanoid';
import { of } from 'rxjs';
import {
  Classroom,
  ClassroomDocument,
} from 'src/classrooms/entities/classroom.entity';
import { Group, GroupDocument } from 'src/groups/entities/group.entity';
import { GroupsService } from 'src/groups/groups.service';
import { Task, TaskDocument } from 'src/tasks/entities/task.entity';
import { UserDocument } from 'src/users/schemas/user.schema';
import { CreateCourseDto } from './dto/create-course.dto';
import { DeleteCourseDto } from './dto/delete-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';
import { UpdateCourseGradeDto } from './dto/updateCourseGrade.dto';
import { Course, CourseDocument } from './entities/course.entity';

@Injectable()
export class CoursesService {
  constructor(
    @InjectModel(Course.name)
    private courseModel: Model<CourseDocument>,
    @InjectModel(Classroom.name)
    private classroomModel: Model<ClassroomDocument>,
    @InjectModel(Task.name)
    private taskModel: Model<TaskDocument>,
    @InjectModel(Group.name)
    private groupModel: Model<GroupDocument>,
    private readonly groupsService: GroupsService,
  ) {}

  async create(createCourseDto: CreateCourseDto, user: UserDocument) {
    const classroom = await this.classroomModel
      .findById(createCourseDto.classroom)
      .exec();

    if (classroom.classhead.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    let urlCode: string;
    while (true) {
      urlCode = nanoid(16);
      const cr = await this.courseModel.findOne({ urlCode }).exec();
      if (!cr) break;
    }

    const course = new this.courseModel({
      classroom: Types.ObjectId(createCourseDto.classroom),
      createdDate: new Date().toISOString(),
      isPrivate: createCourseDto.isPrivate,
      name: createCourseDto.name,
      privateStudents: createCourseDto.privateStudents?.map((st) => {
        return Types.ObjectId(st);
      }),
      teacher: Types.ObjectId(createCourseDto.teacher),
      urlCode: urlCode,
      gradeType: createCourseDto.gradeType,
    } as Course);
    classroom.courses.push(course._id);
    await course.save();
    await classroom.save();
    return this.courseModel.findById(course._id).select({ __v: false }).exec();
  }

  async deleteCourse(
    courseId: string,
    deleteCourseDto: DeleteCourseDto,
    user: UserDocument,
  ) {
    const classroom = await this.classroomModel
      .findById(deleteCourseDto.classroom)
      .exec();
    const deletedCourse = await this.courseModel
      .findOne({
        _id: courseId,
        classroom: deleteCourseDto.classroom,
      })
      .exec();

    if (classroom.classhead.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    if (!deletedCourse) {
      throw new BadRequestException('no course');
    }

    classroom.courses = (classroom.courses as Types.ObjectId[]).filter(
      (t) => t.toString() !== courseId,
    );
    await classroom.save();
    // const deletedCourse = await this.courseModel.findById(courseId).exec();

    // delete tasks
    await this.taskModel.deleteMany({ course: deletedCourse.id }).exec();
    // delete groups
    const courseGroups = await this.groupModel
      .find({ course: deletedCourse._id })
      .exec();
    for (const group of courseGroups) {
      await this.groupsService.deleteGroup(
        group._id.toString(),
        { course: courseId },
        user,
      );
    }

    await this.courseModel.findByIdAndDelete(courseId).exec();
    return deletedCourse;
  }

  async getInfoForCoursePage(
    user: UserDocument,
    courseUrlCode: string,
    classroomUrlCode: string,
  ) {
    const course = await this.courseModel.findOne({ urlCode: courseUrlCode });
    const classroom = await this.classroomModel.findOne({
      courses: { $all: [course._id] },
    });

    if (!course || classroom.urlCode !== classroomUrlCode) {
      throw new BadRequestException('no course');
    }
    if (!classroom) {
      throw new BadRequestException('no classroom');
    }

    let role: string;
    if (course.teacher.toString() === user._id.toString()) {
      role = 'courseteacher';
    } else if (
      (course.isPrivate && course.privateStudents.includes(user._id)) ||
      (!course.isPrivate && classroom.students.includes(user._id))
    ) {
      role = 'student';
    } else if (
      classroom.teachers.includes(user._id) ||
      classroom.classhead.toString() === user._id.toString()
    ) {
      role = 'teacher';
    }
    if (!role) {
      throw new ForbiddenException();
    }

    const query = this.courseModel.findById(course._id).select({ __v: false });
    const courseToReturn = await query
      .populate({ path: 'teacher', select: '-__v -passwordHash' })
      .populate({ path: 'privateStudents', select: '-__v -passwordHash' })
      // TODO: Populate groups here ?
      .exec();

    if (role === 'student') {
      courseToReturn.grades = courseToReturn.grades.filter((grade) => {
        return grade.student.toString() === user._id.toString();
      });
    }

    return {
      course: courseToReturn,
      role: role,
    };
  }

  async update(
    id: string,
    createCourseDto: CreateCourseDto,
    user: UserDocument,
  ) {
    const classroom = await this.classroomModel
      .findById(createCourseDto.classroom)
      .exec();

    if (classroom.classhead.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    const course = await this.courseModel.findById(id).exec();

    if (createCourseDto.name) {
      course.name = createCourseDto.name;
    }
    if (createCourseDto.teacher) {
      course.teacher = Types.ObjectId(createCourseDto.teacher);
    }
    if (createCourseDto.gradeType) {
      course.gradeType = createCourseDto.gradeType;
    }
    if (
      createCourseDto.isPrivate !== undefined ||
      createCourseDto.isPrivate !== null
    ) {
      course.isPrivate = createCourseDto.isPrivate;
    }
    if (createCourseDto.privateStudents) {
      course.privateStudents = createCourseDto.privateStudents?.map((st) => {
        return Types.ObjectId(st);
      });
    }

    // const course = new this.courseModel({
    //   classroom: Types.ObjectId(createCourseDto.classroom),
    //   createdDate: new Date().toISOString(),
    //   isPrivate: createCourseDto.isPrivate,
    //   name: createCourseDto.name,
    //   privateStudents: createCourseDto.privateStudents?.map((st) => {
    //     return Types.ObjectId(st);
    //   }),
    //   teacher: Types.ObjectId(createCourseDto.teacher),
    //   urlCode: urlCode,
    //   gradeType: createCourseDto.gradeType,
    // } as Course);
    // classroom.courses.push(course._id);
    await course.save();
    // await classroom.save();
    return this.courseModel.findById(course._id).select({ __v: false }).exec();
  }

  async updateGrade(
    updateCourseGradeDto: UpdateCourseGradeDto,
    user: UserDocument,
  ) {
    const course = await this.courseModel
      .findById(updateCourseGradeDto.course)
      .exec();

    if (course.teacher.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    const grade = course.grades.find((grade) => {
      return (
        grade.student.toString() === updateCourseGradeDto.student.toString()
      );
    });

    if (grade) {
      grade.grade = updateCourseGradeDto.grade;
    } else {
      course.grades.push({
        student: Types.ObjectId(updateCourseGradeDto.student),
        grade: updateCourseGradeDto.grade,
      });
    }

    await course.save();
  }

  // async findAllByClassroom(user: UserDocument, classroomUrlCode: string) {
  //   const classroom = await this.classroomModel.findOne({
  //     urlCode: classroomUrlCode,
  //   });

  //   let role: string;
  //   if (classroom.classhead.toString() === user._id.toString()) {
  //     role = 'classhead';
  //   } else if (classroom.students.includes(user._id)) {
  //     role = 'student';
  //   } else if (classroom.students.includes(user._id)) {
  //     role = 'teacher';
  //   }
  //   if (!role) {
  //     throw new ForbiddenException();
  //   }

  //   let query = this.classroomModel
  //     .findById(classroom._id)
  //     .select({ __v: false });

  //   if (role !== 'classhead') {
  //     query = query.select({ code: false });
  //   }
  //   const classroomToReturn = await query
  //     .populate({ path: 'classhead', select: '-__v -passwordHash' })
  //     .populate({ path: 'students', select: '-__v -passwordHash' }) // TODO remove ?
  //     .populate({ path: 'teachers', select: '-__v -passwordHash' }) // TODO remove ?
  //     .exec();
  //   // .populate({ path: 'courses', select: '_id name urlCode' }); TODO
  //   return {
  //     classroom: classroomToReturn,
  //     role: role,
  //   };
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} course`;
  // }

  // update(id: number, updateCourseDto: UpdateCourseDto) {
  //   return `This action updates a #${id} course`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} course`;
  // }
}

import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Request,
  UseGuards,
  Put,
} from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { CoursesService } from './courses.service';
import { CreateCourseDto } from './dto/create-course.dto';
import { DeleteCourseDto } from './dto/delete-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';
import { UpdateCourseGradeDto } from './dto/updateCourseGrade.dto';

@Controller('courses')
@UseGuards(JwtAuthGuard)
export class CoursesController {
  constructor(private readonly coursesService: CoursesService) {}

  @Post()
  create(@Request() req, @Body() createCourseDto: CreateCourseDto) {
    return this.coursesService.create(createCourseDto, req.user);
  }

  @Put(':id')
  update(
    @Param('id') id: string,
    @Request() req,
    @Body() createCourseDto: CreateCourseDto,
  ) {
    return this.coursesService.update(id, createCourseDto, req.user);
  }

  @Post(':id/delete')
  delete(
    @Param('id') id: string,
    @Request() req,
    @Body() deleteCourseDto: DeleteCourseDto,
  ) {
    return this.coursesService.deleteCourse(id, deleteCourseDto, req.user);
  }

  @Get(':classroomUrlCode/c/:courseUrlCode')
  getInfoForCoursePage(
    @Request() req,
    @Param('courseUrlCode') courseUrlCode: string,
    @Param('classroomUrlCode') classroomUrlCode: string,
  ) {
    return this.coursesService.getInfoForCoursePage(
      req.user,
      courseUrlCode,
      classroomUrlCode,
    );
  }

  @Patch('grade')
  updateGrade(
    @Request() req,
    @Body() updateCourseGradeDto: UpdateCourseGradeDto,
  ) {
    return this.coursesService.updateGrade(updateCourseGradeDto, req.user);
  }

  // @Get()
  // findAllByClassroom(
  //   @Request() req,
  //   @Param('classroomUrlCode') classroomUrlCode: string,
  // ) {
  //   return this.coursesService.findAllByClassroom();
  // }

  // @Get(':id')
  // findOne(@Request() req, @Param('id') id: string) {
  //   return this.coursesService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateCourseDto: UpdateCourseDto) {
  //   return this.coursesService.update(+id, updateCourseDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.coursesService.remove(+id);
  // }
}

import * as mongoose from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { User } from 'src/users/schemas/user.schema';
import { Classroom } from 'src/classrooms/entities/classroom.entity';
import { Grade, GradeSchema } from 'src/tasks/entities/task.entity';
import { GradeCalculationType } from 'src/enums/grade-calculation.enum';

export type CourseDocument = Course & Document;

@Schema()
export class Course {
  @Prop({ required: true, trim: true })
  name: string;

  @Prop({ trim: true })
  description?: string;

  @Prop({ required: true })
  createdDate: string;

  @Prop({ required: true, unique: true })
  urlCode: string;

  @Prop({
    required: true,
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Classroom',
  })
  classroom: Classroom | mongoose.Types.ObjectId;

  @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  teacher: User | mongoose.Types.ObjectId;

  @Prop({ required: true, default: false })
  isPrivate: boolean;

  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }] })
  privateStudents: User[] | mongoose.Types.ObjectId[];

  @Prop({
    required: true,
    trim: true,
    enum: [GradeCalculationType.AVERAGE, GradeCalculationType.SUM],
  })
  gradeType: GradeCalculationType;

  @Prop({ type: [{ type: GradeSchema }] })
  grades: Grade[];
}

export const CourseSchema = SchemaFactory.createForClass(Course);

import { Module } from '@nestjs/common';
import { CoursesService } from './courses.service';
import { CoursesController } from './courses.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from 'src/users/users.module';
import { Course, CourseSchema } from './entities/course.entity';
import {
  Classroom,
  ClassroomSchema,
} from 'src/classrooms/entities/classroom.entity';
import { Group, GroupSchema } from 'src/groups/entities/group.entity';
import { Task, TaskSchema } from 'src/tasks/entities/task.entity';
import { GroupsModule } from 'src/groups/groups.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Course.name, schema: CourseSchema },
      { name: Classroom.name, schema: ClassroomSchema },
      { name: Task.name, schema: TaskSchema },
      { name: Group.name, schema: GroupSchema },
    ]),
    UsersModule,
    GroupsModule,
  ],
  controllers: [CoursesController],
  providers: [CoursesService],
  exports: [CoursesService],
})
export class CoursesModule {}

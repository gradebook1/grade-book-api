import {
  ArrayUnique,
  IsArray,
  IsBoolean,
  IsIn,
  IsMongoId,
  IsNotEmpty,
  IsOptional,
  MaxLength,
} from 'class-validator';
import { GradeCalculationType } from 'src/enums/grade-calculation.enum';

export class CreateCourseDto {
  @IsNotEmpty()
  @MaxLength(50)
  name: string;

  @IsNotEmpty()
  @IsMongoId()
  classroom: string;

  @IsNotEmpty()
  @IsMongoId()
  teacher: string;

  @IsNotEmpty()
  @IsBoolean()
  isPrivate: boolean;

  @IsOptional()
  @IsArray()
  @ArrayUnique()
  privateStudents?: string[];

  @IsNotEmpty()
  @IsIn([GradeCalculationType.AVERAGE, GradeCalculationType.SUM])
  gradeType: GradeCalculationType;
}

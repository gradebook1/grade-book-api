import { IsMongoId, IsNotEmpty, IsString } from 'class-validator';

export class DeleteCourseDto {
  @IsNotEmpty()
  @IsString()
  @IsMongoId()
  classroom: string;
}

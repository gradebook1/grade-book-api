import * as mongoose from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Course } from 'src/courses/entities/course.entity';
import { User } from 'src/users/schemas/user.schema';

// Grade embedded schema
export type GradeDocument = Grade & Document;

@Schema()
export class Grade {
  @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  student: User | mongoose.Types.ObjectId;

  @Prop()
  grade?: number;
}

export const GradeSchema = SchemaFactory.createForClass(Grade);

// Task schema
export type TaskDocument = Task & Document;

@Schema()
export class Task {
  @Prop({ required: true, trim: true })
  name: string;

  @Prop({ trim: true })
  description?: string;

  @Prop({ required: true })
  createdDate: string;

  @Prop({ required: true })
  date: string;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Course',
  })
  course?: Course | mongoose.Types.ObjectId; // NOTE: Only for course level tasks

  @Prop({ required: true, default: false })
  isPrivate: boolean;

  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }] })
  privateStudents: User[] | mongoose.Types.ObjectId[];

  @Prop({ type: [{ type: GradeSchema }] })
  grades: Grade[];
}

export const TaskSchema = SchemaFactory.createForClass(Task);

import {
  ArrayUnique,
  IsArray,
  IsBoolean,
  IsDateString,
  IsMongoId,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
} from 'class-validator';

export class UpdateTaskDto {
  @IsNotEmpty()
  @IsString()
  @MaxLength(100)
  name: string;

  @IsNotEmpty()
  @IsString()
  @IsDateString()
  date: string;

  @IsNotEmpty()
  @IsString()
  @IsMongoId()
  course: string;

  @IsNotEmpty()
  @IsBoolean()
  isPrivate: boolean;

  @IsOptional()
  @IsArray()
  @ArrayUnique()
  privateStudents?: string[];
}

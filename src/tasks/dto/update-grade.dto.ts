import {
  IsMongoId,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsPositive,
  IsString,
  Min,
} from 'class-validator';

export class UpdateTaskGradeDto {
  @IsNotEmpty()
  @IsString()
  @IsMongoId()
  student: string;

  @IsOptional()
  @IsNumber()
  // @IsInt()
  @Min(0)
  grade?: number;

  @IsNotEmpty()
  @IsString()
  @IsMongoId()
  course: string;

  @IsNotEmpty()
  @IsString()
  @IsMongoId()
  task: string;
}

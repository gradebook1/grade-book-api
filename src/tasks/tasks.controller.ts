import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Request,
  UseGuards,
  Put,
} from '@nestjs/common';
import { TasksService } from './tasks.service';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { CreateCourseLevelTaskDto } from './dto/create-course-task.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { UpdateTaskGradeDto } from './dto/update-grade.dto';
import { DeleteTaskDto } from './dto/delete-task.dto';

@Controller('tasks')
@UseGuards(JwtAuthGuard)
export class TasksController {
  constructor(private readonly tasksService: TasksService) {}

  // @Post()
  // create(@Request() req, @Body() createTaskDto: CreateTaskDto) {
  //   return this.tasksService.create(createTaskDto, req.user);
  // }

  @Post('course-level')
  createCourseLevel(
    @Request() req,
    @Body() createCourseLevelTaskDto: CreateCourseLevelTaskDto,
  ) {
    return this.tasksService.createCourseLevel(
      createCourseLevelTaskDto,
      req.user,
    );
  }

  @Post('course-level/delete')
  deleteCourseLevelTask(@Request() req, @Body() deleteTaskDto: DeleteTaskDto) {
    return this.tasksService.deleteCourseLevelTask(deleteTaskDto, req.user);
  }

  @Put(':id')
  updateTask(
    @Param('id') id: string,
    @Request() req,
    @Body() updateTaskDto: UpdateTaskDto,
  ) {
    return this.tasksService.update(id, updateTaskDto, req.user);
  }

  @Patch('grade')
  update(@Request() req, @Body() updateTaskGradeDto: UpdateTaskGradeDto) {
    return this.tasksService.updateGrade(updateTaskGradeDto, req.user);
  }

  // @Get()
  // findAll() {
  //   return this.tasksService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.tasksService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateTaskDto: UpdateTaskDto) {
  //   return this.tasksService.update(+id, updateTaskDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.tasksService.remove(+id);
  // }
}

import {
  BadRequestException,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { Course, CourseDocument } from 'src/courses/entities/course.entity';
import { UserDocument } from 'src/users/schemas/user.schema';
import { CreateCourseLevelTaskDto } from './dto/create-course-task.dto';
import { CreateTaskDto } from './dto/create-task.dto';
import { DeleteTaskDto } from './dto/delete-task.dto';
import { UpdateTaskGradeDto } from './dto/update-grade.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { Task, TaskDocument } from './entities/task.entity';

@Injectable()
export class TasksService {
  constructor(
    // @InjectModel(Group.name)
    // private groupModel: Model<GroupDocument>,
    @InjectModel(Course.name)
    private courseModel: Model<CourseDocument>,
    // @InjectModel(Classroom.name)
    // private classroomModel: Model<ClassroomDocument>,
    @InjectModel(Task.name)
    private taskModel: Model<TaskDocument>,
  ) {}

  create(createTaskDto: CreateTaskDto, user: UserDocument) {
    return 'This action adds a new task';
  }

  async createCourseLevel(
    createCourseLevelTaskDto: CreateCourseLevelTaskDto,
    user: UserDocument,
  ) {
    const course = await this.courseModel
      .findById(createCourseLevelTaskDto.course)
      .exec();

    if (course.teacher.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    const task = new this.taskModel({
      course: Types.ObjectId(createCourseLevelTaskDto.course),
      createdDate: new Date().toISOString(),
      name: createCourseLevelTaskDto.name,
      date: createCourseLevelTaskDto.date,
      isPrivate: createCourseLevelTaskDto.isPrivate,
      privateStudents: createCourseLevelTaskDto.privateStudents?.map((st) => {
        return Types.ObjectId(st);
      }),
    } as Task);
    await task.save();
    return this.taskModel.findById(task._id).select({ __v: false }).exec();
  }

  async deleteCourseLevelTask(
    deleteTaskDto: DeleteTaskDto,
    user: UserDocument,
  ) {
    const course = await this.courseModel.findById(deleteTaskDto.course).exec();

    if (!course) {
      throw new BadRequestException('no course');
    }

    if (course.teacher.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    const deletedTask = await this.taskModel
      .findOneAndDelete({ _id: deleteTaskDto.task, course: course._id })
      .exec();
    // await task.save();
    return deletedTask;
  }

  async updateGrade(
    updateTaskGradeDto: UpdateTaskGradeDto,
    user: UserDocument,
  ) {
    const course = await this.courseModel
      .findById(updateTaskGradeDto.course)
      .exec();

    if (course.teacher.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    const task = await this.taskModel
      .findOne({
        _id: updateTaskGradeDto.task,
        // course: updateTaskGradeDto.course, // TODO: If we dont use course for finding task we can use that endpoint to update any task
      })
      .exec();

    const grade = task.grades.find((grade) => {
      return grade.student.toString() === updateTaskGradeDto.student.toString();
    });

    if (grade) {
      grade.grade = updateTaskGradeDto.grade;
    } else {
      task.grades.push({
        student: Types.ObjectId(updateTaskGradeDto.student),
        grade: updateTaskGradeDto.grade,
      });
    }

    await task.save();
  }

  // findAll() {
  //   return `This action returns all tasks`;
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} task`;
  // }

  // update(id: number, updateTaskDto: UpdateTaskDto) {
  //   return `This action updates a #${id} task`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} task`;
  // }

  async update(id: string, updateTaskDto: UpdateTaskDto, user: UserDocument) {
    const course = await this.courseModel.findById(updateTaskDto.course).exec();

    if (course.teacher.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    const task = await this.taskModel.findById(id).exec();

    if (updateTaskDto.name) {
      task.name = updateTaskDto.name;
    }
    if (updateTaskDto.date) {
      task.date = updateTaskDto.date;
    }
    if (
      updateTaskDto.isPrivate !== undefined ||
      updateTaskDto.isPrivate !== null
    ) {
      task.isPrivate = updateTaskDto.isPrivate;
    }
    if (updateTaskDto.privateStudents) {
      task.privateStudents = updateTaskDto.privateStudents?.map((st) => {
        return Types.ObjectId(st);
      });
    }

    await task.save();
    return this.taskModel.findById(task._id).select({ __v: false }).exec();
  }
}

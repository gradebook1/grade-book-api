import {
  BadRequestException,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { nanoid } from 'nanoid';
import { CoursesService } from 'src/courses/courses.service';
import { Course, CourseDocument } from 'src/courses/entities/course.entity';
import { Task, TaskDocument } from 'src/tasks/entities/task.entity';
import { UserDocument } from 'src/users/schemas/user.schema';
import { UsersService } from 'src/users/users.service';
import { CreateClassroomDto } from './dto/create-classroom.dto';
import { JoinClassroomDto } from './dto/join-classroom.dto';
import { UpdateClassroomDto } from './dto/update-classroom.dto';
import { Classroom, ClassroomDocument } from './entities/classroom.entity';

@Injectable()
export class ClassroomsService {
  constructor(
    @InjectModel(Classroom.name)
    private classroomModel: Model<ClassroomDocument>,
    @InjectModel(Course.name)
    private courseModel: Model<CourseDocument>,
    @InjectModel(Task.name)
    private taskModel: Model<TaskDocument>,
    private usersService: UsersService,
    private readonly coursesService: CoursesService,
  ) {}

  async searchForUsers(id: string, username: string, user: UserDocument) {
    const classroom = await this.classroomModel.findById(id).exec();

    if (classroom.classhead.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    const users = await this.usersService.findAllByName(username.trim());
    return users.filter((u) => {
      if (
        classroom.classhead.toString() !== u._id.toString() &&
        !classroom.teachers.includes(u._id) &&
        !classroom.students.includes(u._id)
      ) {
        return true;
      }
      return false;
    });
  }

  // TODO: Does not throw error. WHY???
  async addTeachers(id: string, userIds: string[], user: UserDocument) {
    const classroom = await this.classroomModel.findById(id).exec();

    if (classroom.classhead.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    let isError = false;
    userIds.forEach(async (userId) => {
      const userToAdd = await this.usersService.findById(userId);
      if (
        classroom.classhead.toString() !== userToAdd._id.toString() &&
        !classroom.teachers.includes(userToAdd._id) &&
        !classroom.students.includes(userToAdd._id)
      ) {
        classroom.teachers.push(userToAdd._id);
        await classroom.save();
      } else {
        isError = true;
      }
    });
    // await classroom.save();
    if (isError) throw new BadRequestException();
    return classroom.teachers;
  }

  async deleteTeacher(id: string, teacherId: string, user: UserDocument) {
    const classroom = await this.classroomModel.findById(id).exec();

    if (classroom.classhead.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    // NOTE: IF teacher has courses assigned on him then we cant delete him (or reassign all his courses to classhed)

    classroom.teachers = (classroom.teachers as Types.ObjectId[]).filter(
      (t) => t.toString() !== teacherId,
    );

    const courses = await this.courseModel
      .find({ classroom: classroom._id, teacher: teacherId })
      .exec();
    for (const course of courses) {
      course.teacher = classroom.classhead;
      await course.save();
    }

    await classroom.save();
    return classroom.teachers;
  }

  async deleteStudent(id: string, studentId: string, user: UserDocument) {
    const classroom = await this.classroomModel.findById(id).exec();

    if (classroom.classhead.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    classroom.students = (classroom.students as Types.ObjectId[]).filter(
      (s) => s.toString() !== studentId,
    );

    // NOTE: Need to delete user from all private courses and tasks ? and grades? (dont delete grades)
    const privateCourses = await this.courseModel
      .find({ privateStudents: { $all: [Types.ObjectId(studentId)] } })
      .exec();
    for (const course of privateCourses) {
      course.privateStudents = (course.privateStudents as Types.ObjectId[]).filter(
        (s) => s.toString() !== studentId,
      );
      await course.save();
    }

    const privateTasks = await this.taskModel
      .find({ privateStudents: { $all: [Types.ObjectId(studentId)] } })
      .exec();
    for (const task of privateTasks) {
      task.privateStudents = (task.privateStudents as Types.ObjectId[]).filter(
        (s) => s.toString() !== studentId,
      );
      await task.save();
    }

    await classroom.save();
    return classroom.students;
  }

  async join(joinClassroomDto: JoinClassroomDto, user: UserDocument) {
    const classroom = await this.classroomModel.findOne({
      code: joinClassroomDto.code,
    });
    if (!classroom) {
      throw new BadRequestException('classroom');
    }
    if (classroom.students.includes(user._id)) {
      throw new BadRequestException('already joined');
    }
    if (classroom.teachers.includes(user._id)) {
      throw new BadRequestException('user is teacher');
    }
    if (classroom.classhead.toString() !== user._id.toString()) {
      classroom.students.push(user._id);
      await classroom.save();
      return { name: classroom.name };
    } else {
      throw new BadRequestException('user is classhead');
    }
  }

  async create(createClassroomDto: CreateClassroomDto, classheadId: string) {
    let code: string;
    while (true) {
      code = nanoid(16);
      const cr = await this.classroomModel.findOne({ code }).exec();
      if (!cr) break;
    }

    let urlCode: string;
    while (true) {
      urlCode = nanoid(16);
      const cr = await this.classroomModel.findOne({ urlCode }).exec();
      if (!cr) break;
    }

    const classroom = new this.classroomModel({
      classhead: Types.ObjectId(classheadId),
      createdDate: new Date().toISOString(),
      description: createClassroomDto.description,
      name: createClassroomDto.name,
      students: [],
      teachers: [],
      code: code,
      urlCode: urlCode,
      courses: [],
    } as Classroom);
    await classroom.save();
    return this.classroomModel
      .findById(classroom._id)
      .select({ __v: false, code: false })
      .exec();
  }

  async deleteClassroom(classroomId: string, user: UserDocument) {
    const classroom = await this.classroomModel.findById(classroomId).exec();

    if (!classroom) {
      throw new BadRequestException('no classroom');
    }

    if (classroom.classhead.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    // delete courses
    for (const courseId of classroom.courses) {
      await this.coursesService.deleteCourse(
        courseId.toString(),
        { classroom: classroomId },
        user,
      );
    }

    await this.classroomModel.findByIdAndDelete(classroomId).exec();
    return classroom;
  }

  async update(
    id: string,
    updateClassroomDto: UpdateClassroomDto,
    user: UserDocument,
  ) {
    const classroom = await this.classroomModel.findById(id).exec();

    if (classroom.classhead.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    if (updateClassroomDto.name) {
      classroom.name = updateClassroomDto.name;
    }
    if (updateClassroomDto.description) {
      classroom.description = updateClassroomDto.description;
    }

    await classroom.save();
    return this.classroomModel
      .findById(classroom._id)
      .select({ __v: false })
      .exec();
  }

  async findAllByUser(userId: string) {
    const createdBy = await this.classroomModel
      .find({ classhead: userId })
      .select({ __v: false, code: false })
      .populate({ path: 'classhead', select: '-__v -passwordHash' })
      .exec();
    const whereTeacher = await this.classroomModel
      .find({ teachers: { $all: [userId] } })
      .select({ __v: false, code: false })
      .populate({ path: 'classhead', select: '-__v -passwordHash' })
      .exec();
    const whereStudent = await this.classroomModel
      .find({ students: { $all: [userId] } })
      .select({ __v: false, code: false })
      .populate({ path: 'classhead', select: '-__v -passwordHash' })
      .exec();

    return { createdBy, whereStudent, whereTeacher };
  }

  async getInfoForClassroomPage(user: UserDocument, classroomUrlCode: string) {
    const classroom = await this.classroomModel.findOne({
      urlCode: classroomUrlCode,
    });

    if (!classroom) {
      throw new BadRequestException('no classroom');
    }

    let role: string;
    if (classroom.classhead.toString() === user._id.toString()) {
      role = 'classhead';
    } else if (classroom.students.includes(user._id)) {
      role = 'student';
    } else if (classroom.teachers.includes(user._id)) {
      role = 'teacher';
    }
    if (!role) {
      throw new ForbiddenException();
    }

    let query = this.classroomModel
      .findById(classroom._id)
      .select({ __v: false });

    if (role !== 'classhead') {
      query = query.select({ code: false });
    }
    const classroomToReturn = await query
      .populate({ path: 'classhead', select: '-__v -passwordHash' })
      .populate({ path: 'students', select: '-__v -passwordHash' }) // TODO remove ?
      .populate({ path: 'teachers', select: '-__v -passwordHash' }) // TODO remove ?
      .populate({ path: 'courses', select: '_id name urlCode' })
      .exec();
    return {
      classroom: classroomToReturn,
      role: role,
    };
  }

  // findOne(id: number) {
  //   return `This action returns a #${id} classroom`;
  // }

  // update(id: number, updateClassroomDto: UpdateClassroomDto) {
  //   return `This action updates a #${id} classroom`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} classroom`;
  // }
}

import { Module } from '@nestjs/common';
import { ClassroomsService } from './classrooms.service';
import { ClassroomsController } from './classrooms.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Classroom, ClassroomSchema } from './entities/classroom.entity';
import { UsersModule } from 'src/users/users.module';
import { Course, CourseSchema } from 'src/courses/entities/course.entity';
import { CoursesModule } from 'src/courses/courses.module';
import { Task, TaskSchema } from 'src/tasks/entities/task.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Classroom.name, schema: ClassroomSchema },
      { name: Course.name, schema: CourseSchema },
      { name: Task.name, schema: TaskSchema },
    ]),
    UsersModule,
    CoursesModule,
  ],
  controllers: [ClassroomsController],
  providers: [ClassroomsService],
  exports: [ClassroomsService],
})
export class ClassroomsModule {}

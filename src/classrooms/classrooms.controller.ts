import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Request,
  Put,
} from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { ClassroomsService } from './classrooms.service';
import { CreateClassroomDto } from './dto/create-classroom.dto';
import { JoinClassroomDto } from './dto/join-classroom.dto';
import { UpdateClassroomDto } from './dto/update-classroom.dto';

@Controller('classrooms')
@UseGuards(JwtAuthGuard)
export class ClassroomsController {
  constructor(private readonly classroomsService: ClassroomsService) {}

  @Post()
  create(@Request() req, @Body() createClassroomDto: CreateClassroomDto) {
    return this.classroomsService.create(createClassroomDto, req.user._id);
  }

  @Put(':id')
  update(
    @Param('id') id: string,
    @Request() req,
    @Body() updateClassroomDto: UpdateClassroomDto,
  ) {
    return this.classroomsService.update(id, updateClassroomDto, req.user);
  }

  @Post(':id/delete')
  delete(@Param('id') id: string, @Request() req) {
    return this.classroomsService.deleteClassroom(id, req.user);
  }

  @Get()
  findAll(@Request() req) {
    return this.classroomsService.findAllByUser(req.user._id);
  }

  @Post(':id/searchusers')
  findAllUsersByName(
    @Request() req,
    @Param('id') id: string,
    @Body() dto: { username: string },
  ) {
    return this.classroomsService.searchForUsers(id, dto.username, req.user);
  }

  @Post(':id/teachers')
  addTeachers(
    @Request() req,
    @Param('id') id: string,
    @Body() dto: { userIds: string[] },
  ) {
    return this.classroomsService.addTeachers(id, dto.userIds, req.user);
  }

  @Post(':id/teachers/delete')
  deleteTeacher(
    @Request() req,
    @Param('id') id: string,
    @Body() dto: { teacherId: string },
  ) {
    return this.classroomsService.deleteTeacher(id, dto.teacherId, req.user);
  }

  @Post(':id/students/delete')
  deleteStudent(
    @Request() req,
    @Param('id') id: string,
    @Body() dto: { studentId: string },
  ) {
    return this.classroomsService.deleteStudent(id, dto.studentId, req.user);
  }

  @Post('join')
  join(@Request() req, @Body() joinClassroomDto: JoinClassroomDto) {
    return this.classroomsService.join(joinClassroomDto, req.user);
  }

  @Get(':classroomUrlCode')
  getInfoForClassroomPage(
    @Request() req,
    @Param('classroomUrlCode') classroomUrlCode: string,
  ) {
    return this.classroomsService.getInfoForClassroomPage(
      req.user,
      classroomUrlCode,
    );
  }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.classroomsService.findOne(+id);
  // }

  // @Patch(':id')
  // update(
  //   @Param('id') id: string,
  //   @Body() updateClassroomDto: UpdateClassroomDto,
  // ) {
  //   return this.classroomsService.update(+id, updateClassroomDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.classroomsService.remove(+id);
  // }
}

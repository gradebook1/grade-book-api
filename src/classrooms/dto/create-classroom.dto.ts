import { IsNotEmpty, MaxLength } from 'class-validator';

export class CreateClassroomDto {
  @IsNotEmpty()
  @MaxLength(50)
  name: string;

  @IsNotEmpty()
  @MaxLength(100)
  description: string;
}

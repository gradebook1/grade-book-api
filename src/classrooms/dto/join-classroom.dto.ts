import { IsNotEmpty } from 'class-validator';

export class JoinClassroomDto {
  @IsNotEmpty()
  code: string;
}

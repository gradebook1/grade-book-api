import { IsNotEmpty, MaxLength } from 'class-validator';

export class UpdateClassroomDto {
  @IsNotEmpty()
  @MaxLength(50)
  name: string;

  @IsNotEmpty()
  @MaxLength(100)
  description: string;
}

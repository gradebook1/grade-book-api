import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import * as cookieParser from 'cookie-parser';
import { AppModule } from './app.module';
import * as mongoose from 'mongoose';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors({
    // DEV: For development
    // origin: ['http://localhost:3000', 'https://gradebook1.gitlab.io'],
    origin: ['https://gradebook1.gitlab.io'],
    credentials: true,
  });
  app.use(cookieParser());
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
    }),
  );
  // mongoose.set('debug', true);
  await app.listen(process.env.PORT || 3001);
}
bootstrap();

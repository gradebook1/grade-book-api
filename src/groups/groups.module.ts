import { Module } from '@nestjs/common';
import { GroupsService } from './groups.service';
import { GroupsController } from './groups.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from 'src/users/users.module';
import { Course, CourseSchema } from 'src/courses/entities/course.entity';
import {
  Classroom,
  ClassroomSchema,
} from 'src/classrooms/entities/classroom.entity';
import { Group, GroupSchema } from './entities/group.entity';
import { Task, TaskSchema } from 'src/tasks/entities/task.entity';
import { SubgroupsModule } from 'src/subgroups/subgroups.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Group.name, schema: GroupSchema },
      { name: Course.name, schema: CourseSchema },
      { name: Classroom.name, schema: ClassroomSchema },
      { name: Task.name, schema: TaskSchema },
    ]),
    UsersModule,
    SubgroupsModule,
  ],
  controllers: [GroupsController],
  providers: [GroupsService],
  exports: [GroupsService],
})
export class GroupsModule {}

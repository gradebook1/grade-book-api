import {
  BadRequestException,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import {
  Classroom,
  ClassroomDocument,
} from 'src/classrooms/entities/classroom.entity';
import { Course, CourseDocument } from 'src/courses/entities/course.entity';
import { SubgroupsService } from 'src/subgroups/subgroups.service';
import { CreateTaskDto } from 'src/tasks/dto/create-task.dto';
import { DeleteTaskDto } from 'src/tasks/dto/delete-task.dto';
import { Task, TaskDocument } from 'src/tasks/entities/task.entity';
import { User, UserDocument } from 'src/users/schemas/user.schema';
import { CreateGroupDto } from './dto/create-group.dto';
import { DeleteGroupDto } from './dto/delete-group.dto';
import { UpdateGroupGradeDto } from './dto/update-group-grade.dto';
import { UpdateGroupDto } from './dto/update-group.dto';
import { Group, GroupDocument } from './entities/group.entity';

@Injectable()
export class GroupsService {
  constructor(
    @InjectModel(Group.name)
    private groupModel: Model<GroupDocument>,
    @InjectModel(Course.name)
    private courseModel: Model<CourseDocument>,
    @InjectModel(Classroom.name)
    private classroomModel: Model<ClassroomDocument>,
    @InjectModel(Task.name)
    private taskModel: Model<TaskDocument>,
    private readonly subgroupsService: SubgroupsService,
  ) {}

  async updateGrade(
    updateGroupGradeDto: UpdateGroupGradeDto,
    user: UserDocument,
  ) {
    const course = await this.courseModel
      .findById(updateGroupGradeDto.course)
      .exec();

    const group = await this.groupModel
      .findById(updateGroupGradeDto.group)
      .exec();

    if (course.teacher.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    if (!group || group.course.toString() !== course._id.toString()) {
      throw new BadRequestException('no group');
    }

    const grade = group.grades.find((grade) => {
      return (
        grade.student.toString() === updateGroupGradeDto.student.toString()
      );
    });

    if (grade) {
      grade.grade = updateGroupGradeDto.grade;
    } else {
      group.grades.push({
        student: Types.ObjectId(updateGroupGradeDto.student),
        grade: updateGroupGradeDto.grade,
      });
    }

    await group.save();
  }

  async create(createGroupDto: CreateGroupDto, user: UserDocument) {
    const course = await this.courseModel
      .findById(createGroupDto.course)
      .exec();

    if (course.teacher.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    const group = new this.groupModel({
      course: Types.ObjectId(createGroupDto.course),
      createdDate: new Date().toISOString(),
      name: createGroupDto.name,
      gradeType: createGroupDto.gradeType,
      subgroups: [],
      tasks: [],
    } as Group);
    await group.save();
    return this.groupModel.findById(group._id).select({ __v: false }).exec();
  }

  async deleteGroup(
    groupId: string,
    deleteGroupDto: DeleteGroupDto,
    user: UserDocument,
  ) {
    const course = await this.courseModel
      .findById(deleteGroupDto.course)
      .exec();

    if (course.teacher.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    const deletedGroup = await this.groupModel.findById(groupId).exec();

    // delete tasks
    for (const taskId of deletedGroup.tasks) {
      await this.taskModel.findByIdAndDelete(taskId).exec();
    }
    // delete subgroups
    for (const subgroupId of deletedGroup.subgroups) {
      await this.subgroupsService.deleteSubgroup(
        subgroupId.toString(),
        { course: deleteGroupDto.course, group: groupId },
        user,
      );
    }

    await this.groupModel.findByIdAndDelete(groupId).exec();
    return deletedGroup;
  }

  async update(id: string, createGroupDto: CreateGroupDto, user: UserDocument) {
    const course = await this.courseModel
      .findById(createGroupDto.course)
      .exec();

    if (course.teacher.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    const group = await this.groupModel.findById(id).exec();

    if (createGroupDto.name) {
      group.name = createGroupDto.name;
    }
    if (createGroupDto.gradeType) {
      group.gradeType = createGroupDto.gradeType;
    }

    await group.save();
    return this.groupModel.findById(group._id).select({ __v: false }).exec();
  }

  async createTask(
    id: string,
    createTaskDto: CreateTaskDto,
    user: UserDocument,
  ) {
    const course = await this.courseModel.findById(createTaskDto.course).exec();
    const group = await this.groupModel.findById(id).exec();

    if (course.teacher.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    if (!group) {
      throw new BadRequestException('no group');
    }

    const task = new this.taskModel({
      createdDate: new Date().toISOString(),
      name: createTaskDto.name,
      date: createTaskDto.date,
      isPrivate: createTaskDto.isPrivate,
      privateStudents: createTaskDto.privateStudents?.map((st) => {
        return Types.ObjectId(st);
      }),
    } as Task);
    group.tasks.push(task._id);
    await task.save();
    await group.save();
    return this.taskModel.findById(task._id).select({ __v: false }).exec();
  }

  async deleteTask(
    id: string,
    deleteTaskDto: DeleteTaskDto,
    user: UserDocument,
  ) {
    const course = await this.courseModel.findById(deleteTaskDto.course).exec();
    const group = await this.groupModel.findById(id).exec();

    if (course.teacher.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    if (!group) {
      throw new BadRequestException('no group');
    }

    group.tasks = (group.tasks as Types.ObjectId[]).filter(
      (t) => t.toString() !== deleteTaskDto.task.toString(),
    );
    await group.save();
    const deletedTask = await this.taskModel
      .findByIdAndDelete(deleteTaskDto.task)
      .exec();
    // await task.save();
    return deletedTask;
  }

  async findAllByCourse(
    user: UserDocument,
    courseUrlCode: string,
    classroomUrlCode: string,
  ) {
    const course = await this.courseModel.findOne({ urlCode: courseUrlCode });
    const classroom = await this.classroomModel.findOne({
      courses: { $all: [course._id] },
    });

    if (!course || classroom.urlCode !== classroomUrlCode) {
      throw new BadRequestException('no course');
    }
    if (!classroom) {
      throw new BadRequestException('no classroom');
    }

    let role: string;
    if (course.teacher.toString() === user._id.toString()) {
      role = 'courseteacher';
    } else if (
      (course.isPrivate && course.privateStudents.includes(user._id)) ||
      (!course.isPrivate && classroom.students.includes(user._id))
    ) {
      role = 'student';
    } else if (
      classroom.teachers.includes(user._id) ||
      classroom.classhead.toString() === user._id.toString()
    ) {
      role = 'teacher';
    }
    if (!role) {
      throw new ForbiddenException();
    }

    let groups: any[];
    let tasks: any[];

    if (role === 'courseteacher' || role === 'teacher') {
      groups = await this.groupModel
        .find({ course: course._id })
        .populate({
          path: 'tasks',
          select: '-__v',
          populate: { path: 'privateStudents', select: '-__v -passwordHash' },
        })
        .populate({
          path: 'subgroups',
          select: '-__v',
          populate: [
            {
              path: 'tasks',
              select: '-__v',
              populate: {
                path: 'privateStudents',
                select: '-__v -passwordHash',
              },
            },
            {
              path: 'modules',
              select: '-__v',
              populate: {
                path: 'tasks',
                select: '-__v',
                populate: {
                  path: 'privateStudents',
                  select: '-__v -passwordHash',
                },
              },
            },
          ],
        })
        .exec();
      tasks = await this.taskModel
        .find({ course: course._id })
        .populate({ path: 'privateStudents', select: '-__v -passwordHash' })
        .exec();
    } else {
      // If user is a student TODO: Dont return private tasks where user is not private student
      // TODO: Return only user's grades
      groups = await this.groupModel
        .find({ course: course._id })
        .populate({
          path: 'tasks',
          select: '-__v',
          populate: { path: 'privateStudents', select: '-__v -passwordHash' },
        })
        .populate({
          path: 'subgroups',
          select: '-__v',
          populate: [
            {
              path: 'tasks',
              select: '-__v',
              populate: {
                path: 'privateStudents',
                select: '-__v -passwordHash',
              },
            },
            {
              path: 'modules',
              select: '-__v',
              populate: {
                path: 'tasks',
                select: '-__v',
                populate: {
                  path: 'privateStudents',
                  select: '-__v -passwordHash',
                },
              },
            },
          ],
        })
        .exec();
      tasks = await this.taskModel
        .find({ course: course._id })
        .populate({ path: 'privateStudents', select: '-__v -passwordHash' })
        .exec();

      tasks = tasks.filter((task) => {
        if (!task.isPrivate) {
          return true;
        }
        if (
          task.privateStudents.find(
            (ps) => ps._id.toString() === user._id.toString(),
          )
        ) {
          return true;
        } else {
          return false;
        }
      });
      tasks.forEach((task) => {
        task.grades = task.grades.filter((grade) => {
          return grade.student.toString() === user._id.toString();
        });
      });

      groups = filterGroupsByUser(groups, user);
    }

    return { groups, tasks };
  }

  // findAll() {
  //   return `This action returns all groups`;
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} group`;
  // }

  // update(id: number, updateGroupDto: UpdateGroupDto) {
  //   return `This action updates a #${id} group`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} group`;
  // }
}

function filterTasksByUser(tasks: any[], user: UserDocument) {
  tasks = tasks.filter((task) => {
    if (!task.isPrivate) {
      return true;
    }
    if (
      task.privateStudents.find(
        (ps) => ps._id.toString() === user._id.toString(),
      )
    ) {
      return true;
    } else {
      return false;
    }
  });
  tasks.forEach((task) => {
    task.grades = task.grades.filter((grade) => {
      return grade.student.toString() === user._id.toString();
    });
  });

  return tasks;
}

function filterGroupsByUser(groups: any[], user: UserDocument) {
  groups.forEach((group) => {
    group.tasks = filterTasksByUser(group.tasks, user);
    group.subgroups = filterSubgroupsByUser(group.subgroups, user);
    group.grades = group.grades.filter((grade) => {
      return grade.student.toString() === user._id.toString();
    });
  });

  return groups;
}

function filterSubgroupsByUser(subgroups: any[], user: UserDocument) {
  subgroups.forEach((subgroup) => {
    subgroup.tasks = filterTasksByUser(subgroup.tasks, user);
    subgroup.modules = filterModulesByUser(subgroup.modules, user);
    subgroup.grades = subgroup.grades.filter((grade) => {
      return grade.student.toString() === user._id.toString();
    });
  });

  return subgroups;
}

function filterModulesByUser(modules: any[], user: UserDocument) {
  modules.forEach((module) => {
    module.tasks = filterTasksByUser(module.tasks, user);
    module.grades = module.grades.filter((grade) => {
      return grade.student.toString() === user._id.toString();
    });
  });

  return modules;
}

import {
  IsIn,
  IsMongoId,
  IsNotEmpty,
  IsString,
  MaxLength,
} from 'class-validator';
import { GradeCalculationType } from 'src/enums/grade-calculation.enum';

export class CreateGroupDto {
  @IsNotEmpty()
  @IsString()
  @MaxLength(50)
  name: string;

  @IsNotEmpty()
  @IsString()
  @IsMongoId()
  course: string;

  @IsNotEmpty()
  @IsString()
  @IsIn([GradeCalculationType.AVERAGE, GradeCalculationType.SUM])
  gradeType: GradeCalculationType;

  // @IsNotEmpty()
  // @IsBoolean()
  // isPrivate: boolean;

  // @IsOptional()
  // @IsArray()
  // @ArrayUnique()
  // privateStudents: string[];

  // @IsNotEmpty()
  // @IsBoolean()
  // isGroupOfModules: boolean;
}

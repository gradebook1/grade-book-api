import { IsMongoId, IsNotEmpty, IsString } from 'class-validator';

export class DeleteGroupDto {
  @IsNotEmpty()
  @IsString()
  @IsMongoId()
  course: string;
}

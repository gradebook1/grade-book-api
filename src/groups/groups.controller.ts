import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Request,
  UseGuards,
  Put,
} from '@nestjs/common';
import { GroupsService } from './groups.service';
import { CreateGroupDto } from './dto/create-group.dto';
import { UpdateGroupDto } from './dto/update-group.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { CreateTaskDto } from 'src/tasks/dto/create-task.dto';
import { UpdateGroupGradeDto } from './dto/update-group-grade.dto';
import { DeleteTaskDto } from 'src/tasks/dto/delete-task.dto';
import { DeleteGroupDto } from './dto/delete-group.dto';

@Controller('groups')
@UseGuards(JwtAuthGuard)
export class GroupsController {
  constructor(private readonly groupsService: GroupsService) {}

  @Post()
  create(@Request() req, @Body() createGroupDto: CreateGroupDto) {
    return this.groupsService.create(createGroupDto, req.user);
  }

  @Put(':id')
  update(
    @Param('id') id: string,
    @Request() req,
    @Body() createGroupDto: CreateGroupDto,
  ) {
    return this.groupsService.update(id, createGroupDto, req.user);
  }

  @Post(':id/delete')
  delete(
    @Param('id') id: string,
    @Request() req,
    @Body() deleteGroupDto: DeleteGroupDto,
  ) {
    return this.groupsService.deleteGroup(id, deleteGroupDto, req.user);
  }

  @Get(':classroomUrlCode/c/:courseUrlCode')
  findAll(
    @Request() req,
    @Param('courseUrlCode') courseUrlCode: string,
    @Param('classroomUrlCode') classroomUrlCode: string,
  ) {
    return this.groupsService.findAllByCourse(
      req.user,
      courseUrlCode,
      classroomUrlCode,
    );
  }

  @Post(':id/tasks')
  createTask(
    @Request() req,
    @Param('id') id: string,
    @Body() createTaskDto: CreateTaskDto,
  ) {
    return this.groupsService.createTask(id, createTaskDto, req.user);
  }

  @Post(':id/tasks/delete')
  deleteTask(
    @Request() req,
    @Param('id') id: string,
    @Body() deleteTaskDto: DeleteTaskDto,
  ) {
    return this.groupsService.deleteTask(id, deleteTaskDto, req.user);
  }

  @Patch('grade')
  updateGrade(
    @Request() req,
    @Body() updateGroupGradeDto: UpdateGroupGradeDto,
  ) {
    return this.groupsService.updateGrade(updateGroupGradeDto, req.user);
  }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.groupsService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateGroupDto: UpdateGroupDto) {
  //   return this.groupsService.update(+id, updateGroupDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.groupsService.remove(+id);
  // }
}

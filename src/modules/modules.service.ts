import {
  BadRequestException,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { Course, CourseDocument } from 'src/courses/entities/course.entity';
import {
  Subgroup,
  SubgroupDocument,
} from 'src/subgroups/entities/subgroup.entity';
import { CreateTaskDto } from 'src/tasks/dto/create-task.dto';
import { DeleteTaskDto } from 'src/tasks/dto/delete-task.dto';
import { Task, TaskDocument } from 'src/tasks/entities/task.entity';
import { UserDocument } from 'src/users/schemas/user.schema';
import { CreateModuleDto } from './dto/create-module.dto';
import { DeleteModuleDto } from './dto/delete-module.dto';
import { UpdateModuleGradeDto } from './dto/update-module-grade.dto';
import { UpdateModuleDto } from './dto/update-module.dto';
import { Module, ModuleDocument } from './entities/module.entity';

@Injectable()
export class ModulesService {
  constructor(
    @InjectModel(Subgroup.name)
    private subgroupModel: Model<SubgroupDocument>,
    @InjectModel(Module.name)
    private moduleModel: Model<ModuleDocument>,
    @InjectModel(Course.name)
    private courseModel: Model<CourseDocument>,
    @InjectModel(Task.name)
    private taskModel: Model<TaskDocument>,
  ) {}

  async create(createModuleDto: CreateModuleDto, user: UserDocument) {
    const course = await this.courseModel
      .findById(createModuleDto.course)
      .exec();

    const subgroup = await this.subgroupModel
      .findById(createModuleDto.subgroup)
      .exec();

    if (course.teacher.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    if (!subgroup) {
      throw new BadRequestException('no subgroup');
    }

    const module = new this.moduleModel({
      createdDate: new Date().toISOString(),
      name: createModuleDto.name,
      gradeType: createModuleDto.gradeType,
      tasks: [],
    } as Module);
    subgroup.modules.push(module._id);
    await module.save();
    await subgroup.save();
    return this.moduleModel.findById(module._id).select({ __v: false }).exec();
  }

  async deleteModule(
    moduleId: string,
    deleteModuleDto: DeleteModuleDto,
    user: UserDocument,
  ) {
    const course = await this.courseModel
      .findById(deleteModuleDto.course)
      .exec();
    const subgroup = await this.subgroupModel
      .findById(deleteModuleDto.subgroup)
      .exec();

    if (course.teacher.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    if (!subgroup) {
      throw new BadRequestException('no subgroup');
    }

    subgroup.modules = (subgroup.modules as Types.ObjectId[]).filter(
      (t) => t.toString() !== moduleId,
    );
    await subgroup.save();
    const deletedModule = await this.moduleModel
      .findByIdAndDelete(moduleId)
      .exec();

    // delete tasks
    for (const taskId of deletedModule.tasks) {
      await this.taskModel.findByIdAndDelete(taskId).exec();
    }
    return deletedModule;
  }

  async update(
    id: string,
    updateModuleDto: UpdateModuleDto,
    user: UserDocument,
  ) {
    const course = await this.courseModel
      .findById(updateModuleDto.course)
      .exec();

    if (course.teacher.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    const module = await this.moduleModel.findById(id).exec();

    if (updateModuleDto.name) {
      module.name = updateModuleDto.name;
    }
    if (updateModuleDto.gradeType) {
      module.gradeType = updateModuleDto.gradeType;
    }

    await module.save();
    return this.moduleModel.findById(module._id).select({ __v: false }).exec();
  }

  async createTask(
    id: string,
    createTaskDto: CreateTaskDto,
    user: UserDocument,
  ) {
    const course = await this.courseModel.findById(createTaskDto.course).exec();
    const module = await this.moduleModel.findById(id).exec();

    if (course.teacher.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    if (!module) {
      throw new BadRequestException('no module');
    }

    const task = new this.taskModel({
      createdDate: new Date().toISOString(),
      name: createTaskDto.name,
      date: createTaskDto.date,
      isPrivate: createTaskDto.isPrivate,
      privateStudents: createTaskDto.privateStudents?.map((st) => {
        return Types.ObjectId(st);
      }),
    } as Task);
    module.tasks.push(task._id);
    await task.save();
    await module.save();
    return this.taskModel.findById(task._id).select({ __v: false }).exec();
  }

  async deleteTask(
    id: string,
    deleteTaskDto: DeleteTaskDto,
    user: UserDocument,
  ) {
    const course = await this.courseModel.findById(deleteTaskDto.course).exec();
    const module = await this.moduleModel.findById(id).exec();

    if (course.teacher.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    if (!module) {
      throw new BadRequestException('no module');
    }

    module.tasks = (module.tasks as Types.ObjectId[]).filter(
      (t) => t.toString() !== deleteTaskDto.task.toString(),
    );
    await module.save();
    const deletedTask = await this.taskModel
      .findByIdAndDelete(deleteTaskDto.task)
      .exec();
    // await task.save();
    return deletedTask;
  }

  async updateGrade(
    updateModuleGradeDto: UpdateModuleGradeDto,
    user: UserDocument,
  ) {
    const course = await this.courseModel
      .findById(updateModuleGradeDto.course)
      .exec();

    const module = await this.moduleModel
      .findById(updateModuleGradeDto.module)
      .exec();

    if (course.teacher.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    if (!module) {
      throw new BadRequestException('no module');
    }

    const grade = module.grades.find((grade) => {
      return (
        grade.student.toString() === updateModuleGradeDto.student.toString()
      );
    });

    if (grade) {
      grade.grade = updateModuleGradeDto.grade;
    } else {
      module.grades.push({
        student: Types.ObjectId(updateModuleGradeDto.student),
        grade: updateModuleGradeDto.grade,
      });
    }

    await module.save();
  }

  // create(createModuleDto: CreateModuleDto) {
  //   return 'This action adds a new module';
  // }

  // findAll() {
  //   return `This action returns all modules`;
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} module`;
  // }

  // update(id: number, updateModuleDto: UpdateModuleDto) {
  //   return `This action updates a #${id} module`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} module`;
  // }
}

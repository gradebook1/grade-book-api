import {
  IsIn,
  IsMongoId,
  IsNotEmpty,
  IsString,
  MaxLength,
} from 'class-validator';
import { GradeCalculationType } from 'src/enums/grade-calculation.enum';

export class CreateModuleDto {
  @IsNotEmpty()
  @IsString()
  @MaxLength(50)
  name: string;

  @IsNotEmpty()
  @IsString()
  @IsMongoId()
  course: string;

  @IsNotEmpty()
  @IsString()
  @IsMongoId()
  subgroup: string;

  @IsNotEmpty()
  @IsString()
  @IsIn([GradeCalculationType.AVERAGE, GradeCalculationType.SUM])
  gradeType: GradeCalculationType;
}

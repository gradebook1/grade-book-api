import { IsMongoId, IsNotEmpty, IsString } from 'class-validator';

export class DeleteModuleDto {
  @IsNotEmpty()
  @IsString()
  @IsMongoId()
  course: string;

  @IsNotEmpty()
  @IsString()
  @IsMongoId()
  subgroup: string;
}

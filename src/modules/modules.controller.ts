import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Request,
  Put,
} from '@nestjs/common';
import { ModulesService } from './modules.service';
import { CreateModuleDto } from './dto/create-module.dto';
import { UpdateModuleDto } from './dto/update-module.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { CreateTaskDto } from 'src/tasks/dto/create-task.dto';
import { UpdateModuleGradeDto } from './dto/update-module-grade.dto';
import { DeleteTaskDto } from 'src/tasks/dto/delete-task.dto';
import { DeleteModuleDto } from './dto/delete-module.dto';

@Controller('modules')
@UseGuards(JwtAuthGuard)
export class ModulesController {
  constructor(private readonly modulesService: ModulesService) {}

  @Post()
  create(@Request() req, @Body() createModuleDto: CreateModuleDto) {
    return this.modulesService.create(createModuleDto, req.user);
  }

  @Put(':id')
  update(
    @Param('id') id: string,
    @Request() req,
    @Body() updateModuleDto: UpdateModuleDto,
  ) {
    return this.modulesService.update(id, updateModuleDto, req.user);
  }

  @Post(':id/delete')
  delete(
    @Param('id') id: string,
    @Request() req,
    @Body() deleteModuleDto: DeleteModuleDto,
  ) {
    return this.modulesService.deleteModule(id, deleteModuleDto, req.user);
  }

  @Post(':id/tasks')
  createTask(
    @Request() req,
    @Param('id') id: string,
    @Body() createTaskDto: CreateTaskDto,
  ) {
    return this.modulesService.createTask(id, createTaskDto, req.user);
  }

  @Post(':id/tasks/delete')
  deleteTask(
    @Request() req,
    @Param('id') id: string,
    @Body() deleteTaskDto: DeleteTaskDto,
  ) {
    return this.modulesService.deleteTask(id, deleteTaskDto, req.user);
  }

  @Patch('grade')
  updateGrade(
    @Request() req,
    @Body() updateModuleGradeDto: UpdateModuleGradeDto,
  ) {
    return this.modulesService.updateGrade(updateModuleGradeDto, req.user);
  }

  // @Post()
  // create(@Body() createModuleDto: CreateModuleDto) {
  //   return this.modulesService.create(createModuleDto);
  // }

  // @Get()
  // findAll() {
  //   return this.modulesService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.modulesService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateModuleDto: UpdateModuleDto) {
  //   return this.modulesService.update(+id, updateModuleDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.modulesService.remove(+id);
  // }
}

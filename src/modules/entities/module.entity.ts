import * as mongoose from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Grade, GradeSchema, Task } from 'src/tasks/entities/task.entity';
import { GradeCalculationType } from 'src/enums/grade-calculation.enum';

export type ModuleDocument = Module & Document;

@Schema()
export class Module {
  @Prop({ required: true, trim: true })
  name: string;

  @Prop({ trim: true })
  description?: string;

  @Prop({ required: true })
  createdDate: string;

  @Prop({
    required: true,
    trim: true,
    enum: [GradeCalculationType.AVERAGE, GradeCalculationType.SUM],
  })
  gradeType: GradeCalculationType;

  // @Prop({
  //   required: true,
  //   type: mongoose.Schema.Types.ObjectId,
  //   ref: 'Course',
  // })
  // course: Course | mongoose.Types.ObjectId;

  // @Prop({ required: true, default: false })
  // isPrivate: boolean;

  // @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }] })
  // privateStudents: User[] | mongoose.Types.ObjectId[];

  // @Prop({ required: true, immutable: true })
  // isGroupOfModules: boolean;

  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Task' }] })
  tasks: Task[] | mongoose.Types.ObjectId[];

  @Prop({ type: [{ type: GradeSchema }] })
  grades: Grade[];
}

export const ModuleSchema = SchemaFactory.createForClass(Module);

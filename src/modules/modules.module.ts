import { Module } from '@nestjs/common';
import { ModulesService } from './modules.service';
import { ModulesController } from './modules.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Course, CourseSchema } from 'src/courses/entities/course.entity';
import {
  Subgroup,
  SubgroupSchema,
} from 'src/subgroups/entities/subgroup.entity';
import { Task, TaskSchema } from 'src/tasks/entities/task.entity';
import { UsersModule } from 'src/users/users.module';
import { ModuleSchema, Module as MyModule } from './entities/module.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: MyModule.name, schema: ModuleSchema },
      { name: Subgroup.name, schema: SubgroupSchema },
      { name: Task.name, schema: TaskSchema },
      { name: Course.name, schema: CourseSchema },
    ]),
    UsersModule,
  ],
  controllers: [ModulesController],
  providers: [ModulesService],
  exports: [ModulesService],
})
export class ModulesModule {}

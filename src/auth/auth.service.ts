import { BadRequestException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import * as bcrypt from 'bcrypt';

import { UsersService } from 'src/users/users.service';
import { CreateUserDTO } from './create-user.dto';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, pass: string): Promise<any> {
    const user = await this.usersService.findOne(email);
    if (!user) {
      throw new BadRequestException('no user');
    }

    const isMatch = await bcrypt.compare(pass, user.passwordHash);

    if (isMatch) {
      return user;
    }
    throw new BadRequestException('password');
  }

  async login(user: any) {
    const payload = { email: user.email, sub: user._id };
    return [
      {
        userId: user._id,
        email: user.email,
        firstName: user.firstName,
        lastName: user.lastName,
        createdDate: user.createdDate,
      },
      this.jwtService.sign(payload),
    ];
    // TODO: set cookie with jwt here, dont return it
  }

  async register(user: CreateUserDTO) {
    return this.usersService.create(user);
  }

  async getProfile(userId: string) {
    return this.usersService.findById(userId);
  }
}

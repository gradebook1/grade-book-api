import {
  Controller,
  Request,
  Get,
  Post,
  UseGuards,
  Body,
  Res,
  Put,
  Patch,
} from '@nestjs/common';
import { Response } from 'express';
import { AppService } from './app.service';
import { AuthService } from './auth/auth.service';
import { CreateUserDTO } from './auth/create-user.dto';
import { JwtAuthGuard } from './auth/jwt-auth.guard';
import { LocalAuthGuard } from './auth/local-auth.guard';
import { UpdatePasswordDTO } from './users/dto/update-password.dto';
import { UpdateUserDTO } from './users/dto/update-user.dto';
import { UsersService } from './users/users.service';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private authService: AuthService,
    private usersService: UsersService,
  ) {}

  @Post('auth/register')
  async register(@Body() user: CreateUserDTO) {
    return this.authService.register(user);
  }

  @UseGuards(LocalAuthGuard)
  @Post('auth/login')
  async login(@Request() req, @Res({ passthrough: true }) res: Response) {
    const [resBody, jwt] = await this.authService.login(req.user);
    // TODO: Add secure flag in production
    // DEV: Remove secure flag
    res.cookie('access_token', jwt, {
      httpOnly: true,
      sameSite: 'none',
      secure: true,
    });
    return resBody;
  }

  @UseGuards(JwtAuthGuard)
  @Post('auth/logout')
  async logout(@Request() req, @Res({ passthrough: true }) res: Response) {
    // TODO: Add secure flag in production
    res.cookie('access_token', '', { httpOnly: true, sameSite: 'lax' });
    return;
  }

  @UseGuards(JwtAuthGuard)
  @Get('profile')
  getProfile(@Request() req) {
    return this.authService.getProfile(req.user._id);
  }

  @UseGuards(JwtAuthGuard)
  @Put('profile')
  update(@Request() req, @Body() updateUserDto: UpdateUserDTO) {
    return this.usersService.update(updateUserDto, req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Patch('profile/password')
  updatePassword(@Request() req, @Body() updatePasswordDto: UpdatePasswordDTO) {
    return this.usersService.updatePassword(updatePasswordDto, req.user);
  }
}

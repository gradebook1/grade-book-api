import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type UserDocument = User & Document;

@Schema()
export class User {
  @Prop({ required: true, unique: true, trim: true })
  email: string;

  @Prop({ required: true, trim: true })
  firstName: string;

  @Prop({ required: true, trim: true })
  lastName: string;

  @Prop({ required: true })
  passwordHash: string;

  @Prop({ required: true })
  createdDate: string;

  // @Prop({ required: true, default: Role.Student })
  // role: Role;
}

export const UserSchema = SchemaFactory.createForClass(User);

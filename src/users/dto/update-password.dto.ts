import { IsAlphanumeric, IsNotEmpty, MinLength } from 'class-validator';

export class UpdatePasswordDTO {
  @IsNotEmpty()
  @IsAlphanumeric()
  @MinLength(6)
  oldPassword: string;

  @IsNotEmpty()
  @IsAlphanumeric()
  @MinLength(6)
  newPassword: string;
}

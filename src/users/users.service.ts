import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as bcrypt from 'bcrypt';
import { CreateUserDTO } from 'src/auth/create-user.dto';
import { User, UserDocument } from './schemas/user.schema';
import { UpdateUserDTO } from './dto/update-user.dto';
import { UpdatePasswordDTO } from './dto/update-password.dto';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}

  async findOne(email: string) {
    return this.userModel.findOne({ email }).exec();
  }

  async findAllByName(username: string) {
    const users = await this.userModel
      .find()
      .or([
        { firstName: username },
        { lastName: username },
        { firstName: username.split(/\s/)[0] },
        { lastName: username.split(/\s/)[1] },
      ])
      .select({ passwordHash: false, __v: false })
      .exec();

    return Array.from(new Set(users));
  }

  async findById(userId: string) {
    return this.userModel
      .findById(userId)
      .select({ passwordHash: false, __v: false })
      .exec();
  }

  async create(createUserDto: CreateUserDTO) {
    let user = await this.userModel
      .findOne({ email: createUserDto.email })
      .exec();

    if (user) {
      throw new BadRequestException('user exists');
    }

    const { password, ...userData } = createUserDto;

    const salt = await bcrypt.genSalt();
    const hash = await bcrypt.hash(password, salt);

    user = new this.userModel({
      ...userData,
      createdDate: new Date().toISOString(),
      passwordHash: hash,
    } as User);

    await user.save();
    return this.userModel
      .findById(user._id)
      .select({ passwordHash: false, __v: false })
      .exec();
  }

  async update(updateUserDto: UpdateUserDTO, user: UserDocument) {
    const { email } = updateUserDto;
    const userWithEmail = await this.userModel.findOne({ email }).exec();

    if (userWithEmail && user._id.toString() !== userWithEmail._id.toString()) {
      throw new BadRequestException('email reserved');
    }

    await this.userModel.findByIdAndUpdate(user._id, updateUserDto).exec();

    return this.userModel
      .findById(user._id)
      .select({ passwordHash: false, __v: false })
      .exec();
  }

  async updatePassword(
    updatePasswordDto: UpdatePasswordDTO,
    user: UserDocument,
  ) {
    const userWithHash = await this.userModel.findById(user._id).exec();
    const isMatch = await bcrypt.compare(
      updatePasswordDto.oldPassword,
      userWithHash.passwordHash,
    );

    if (!isMatch) {
      throw new BadRequestException('password');
    }

    const salt = await bcrypt.genSalt();
    const hash = await bcrypt.hash(updatePasswordDto.newPassword, salt);

    await this.userModel
      .findByIdAndUpdate(user._id, { passwordHash: hash })
      .exec();

    return this.userModel
      .findById(user._id)
      .select({ passwordHash: false, __v: false })
      .exec();
  }
}

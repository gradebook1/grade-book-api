import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Request,
  Put,
} from '@nestjs/common';
import { SubgroupsService } from './subgroups.service';
import { CreateSubgroupDto } from './dto/create-subgroup.dto';
import { UpdateSubgroupDto } from './dto/update-subgroup.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { CreateTaskDto } from 'src/tasks/dto/create-task.dto';
import { UpdateSubgroupGradeDto } from './dto/update-subgroup-grade.dto';
import { DeleteTaskDto } from 'src/tasks/dto/delete-task.dto';
import { DeleteSubgroupDto } from './dto/delete-subgroup.dto';

@Controller('subgroups')
@UseGuards(JwtAuthGuard)
export class SubgroupsController {
  constructor(private readonly subgroupsService: SubgroupsService) {}

  @Post()
  create(@Request() req, @Body() createSubgroupDto: CreateSubgroupDto) {
    return this.subgroupsService.create(createSubgroupDto, req.user);
  }

  @Put(':id')
  update(
    @Param('id') id: string,
    @Request() req,
    @Body() updateSubgroupDto: UpdateSubgroupDto,
  ) {
    return this.subgroupsService.update(id, updateSubgroupDto, req.user);
  }

  @Post(':id/delete')
  delete(
    @Param('id') id: string,
    @Request() req,
    @Body() deleteSubgroupDto: DeleteSubgroupDto,
  ) {
    return this.subgroupsService.deleteSubgroup(
      id,
      deleteSubgroupDto,
      req.user,
    );
  }

  @Post(':id/tasks')
  createTask(
    @Request() req,
    @Param('id') id: string,
    @Body() createTaskDto: CreateTaskDto,
  ) {
    return this.subgroupsService.createTask(id, createTaskDto, req.user);
  }

  @Post(':id/tasks/delete')
  deleteTask(
    @Request() req,
    @Param('id') id: string,
    @Body() deleteTaskDto: DeleteTaskDto,
  ) {
    return this.subgroupsService.deleteTask(id, deleteTaskDto, req.user);
  }

  @Patch('grade')
  updateGrade(
    @Request() req,
    @Body() updateSubgroupGradeDto: UpdateSubgroupGradeDto,
  ) {
    return this.subgroupsService.updateGrade(updateSubgroupGradeDto, req.user);
  }

  // @Post()
  // create(@Body() createSubgroupDto: CreateSubgroupDto) {
  //   return this.subgroupsService.create(createSubgroupDto);
  // }

  // @Get()
  // findAll() {
  //   return this.subgroupsService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.subgroupsService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateSubgroupDto: UpdateSubgroupDto) {
  //   return this.subgroupsService.update(+id, updateSubgroupDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.subgroupsService.remove(+id);
  // }
}

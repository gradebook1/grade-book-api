import { Module } from '@nestjs/common';
import { SubgroupsService } from './subgroups.service';
import { SubgroupsController } from './subgroups.controller';
import { Subgroup, SubgroupSchema } from './entities/subgroup.entity';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from 'src/users/users.module';
import { Task, TaskSchema } from 'src/tasks/entities/task.entity';
import { Course, CourseSchema } from 'src/courses/entities/course.entity';
import { Group, GroupSchema } from 'src/groups/entities/group.entity';
import { ModulesModule } from 'src/modules/modules.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Subgroup.name, schema: SubgroupSchema },
      { name: Group.name, schema: GroupSchema },
      { name: Task.name, schema: TaskSchema },
      { name: Course.name, schema: CourseSchema },
    ]),
    UsersModule,
    ModulesModule,
  ],
  controllers: [SubgroupsController],
  providers: [SubgroupsService],
  exports: [SubgroupsService],
})
export class SubgroupsModule {}

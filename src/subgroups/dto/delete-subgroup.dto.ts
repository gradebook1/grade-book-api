import { IsMongoId, IsNotEmpty, IsString } from 'class-validator';

export class DeleteSubgroupDto {
  @IsNotEmpty()
  @IsString()
  @IsMongoId()
  course: string;

  @IsNotEmpty()
  @IsString()
  @IsMongoId()
  group: string;
}

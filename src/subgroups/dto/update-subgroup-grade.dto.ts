import {
  IsMongoId,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  Min,
} from 'class-validator';

export class UpdateSubgroupGradeDto {
  @IsNotEmpty()
  @IsString()
  @IsMongoId()
  student: string;

  @IsOptional()
  @IsNumber()
  // @IsInt()
  @Min(0)
  grade?: number;

  @IsNotEmpty()
  @IsString()
  @IsMongoId()
  course: string;

  @IsNotEmpty()
  @IsString()
  @IsMongoId()
  subgroup: string;
}

import {
  BadRequestException,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import {
  Classroom,
  ClassroomDocument,
} from 'src/classrooms/entities/classroom.entity';
import { Course, CourseDocument } from 'src/courses/entities/course.entity';
import { Group, GroupDocument } from 'src/groups/entities/group.entity';
import { ModulesService } from 'src/modules/modules.service';
import { CreateTaskDto } from 'src/tasks/dto/create-task.dto';
import { DeleteTaskDto } from 'src/tasks/dto/delete-task.dto';
import { Task, TaskDocument } from 'src/tasks/entities/task.entity';
import { UserDocument } from 'src/users/schemas/user.schema';
import { CreateSubgroupDto } from './dto/create-subgroup.dto';
import { DeleteSubgroupDto } from './dto/delete-subgroup.dto';
import { UpdateSubgroupGradeDto } from './dto/update-subgroup-grade.dto';
import { UpdateSubgroupDto } from './dto/update-subgroup.dto';
import { Subgroup, SubgroupDocument } from './entities/subgroup.entity';

@Injectable()
export class SubgroupsService {
  constructor(
    @InjectModel(Subgroup.name)
    private subgroupModel: Model<SubgroupDocument>,
    @InjectModel(Group.name)
    private groupModel: Model<GroupDocument>,
    @InjectModel(Course.name)
    private courseModel: Model<CourseDocument>,
    @InjectModel(Task.name)
    private taskModel: Model<TaskDocument>,
    private readonly modulesService: ModulesService,
  ) {}

  async create(createSubgroupDto: CreateSubgroupDto, user: UserDocument) {
    const course = await this.courseModel
      .findById(createSubgroupDto.course)
      .exec();

    const group = await this.groupModel
      .findById(createSubgroupDto.group)
      .exec();

    if (course.teacher.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    if (!group || group.course.toString() !== course._id.toString()) {
      throw new BadRequestException('no group');
    }

    const subgroup = new this.subgroupModel({
      createdDate: new Date().toISOString(),
      name: createSubgroupDto.name,
      gradeType: createSubgroupDto.gradeType,
      modules: [],
      tasks: [],
    } as Subgroup);
    group.subgroups.push(subgroup._id);
    await subgroup.save();
    await group.save();
    return this.subgroupModel
      .findById(subgroup._id)
      .select({ __v: false })
      .exec();
  }

  async deleteSubgroup(
    subgroupId: string,
    deleteSubgroupDto: DeleteSubgroupDto,
    user: UserDocument,
  ) {
    const course = await this.courseModel
      .findById(deleteSubgroupDto.course)
      .exec();
    const group = await this.groupModel
      .findById(deleteSubgroupDto.group)
      .exec();

    if (course.teacher.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    if (!group) {
      throw new BadRequestException('no group');
    }

    group.subgroups = (group.subgroups as Types.ObjectId[]).filter(
      (t) => t.toString() !== subgroupId,
    );
    await group.save();
    const deletedSubgroup = await this.subgroupModel
      .findById(subgroupId)
      .exec();

    // delete tasks
    for (const taskId of deletedSubgroup.tasks) {
      await this.taskModel.findByIdAndDelete(taskId).exec();
    }
    // delete modules
    for (const moduleId of deletedSubgroup.modules) {
      await this.modulesService.deleteModule(
        moduleId.toString(),
        { course: deleteSubgroupDto.course, subgroup: subgroupId },
        user,
      );
    }

    await this.subgroupModel.findByIdAndDelete(subgroupId).exec();
    return deletedSubgroup;
  }

  async update(
    id: string,
    updateSubgroupDto: UpdateSubgroupDto,
    user: UserDocument,
  ) {
    const course = await this.courseModel
      .findById(updateSubgroupDto.course)
      .exec();

    if (course.teacher.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    const subgroup = await this.subgroupModel.findById(id).exec();

    if (updateSubgroupDto.name) {
      subgroup.name = updateSubgroupDto.name;
    }
    if (updateSubgroupDto.gradeType) {
      subgroup.gradeType = updateSubgroupDto.gradeType;
    }

    await subgroup.save();
    return this.subgroupModel
      .findById(subgroup._id)
      .select({ __v: false })
      .exec();
  }

  async createTask(
    id: string,
    createTaskDto: CreateTaskDto,
    user: UserDocument,
  ) {
    const course = await this.courseModel.findById(createTaskDto.course).exec();
    const subgroup = await this.subgroupModel.findById(id).exec();

    if (course.teacher.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    if (!subgroup) {
      throw new BadRequestException('no subgroup');
    }

    const task = new this.taskModel({
      createdDate: new Date().toISOString(),
      name: createTaskDto.name,
      date: createTaskDto.date,
      isPrivate: createTaskDto.isPrivate,
      privateStudents: createTaskDto.privateStudents?.map((st) => {
        return Types.ObjectId(st);
      }),
    } as Task);
    subgroup.tasks.push(task._id);
    await task.save();
    await subgroup.save();
    return this.taskModel.findById(task._id).select({ __v: false }).exec();
  }

  async deleteTask(
    id: string,
    deleteTaskDto: DeleteTaskDto,
    user: UserDocument,
  ) {
    const course = await this.courseModel.findById(deleteTaskDto.course).exec();
    const subgroup = await this.subgroupModel.findById(id).exec();

    if (course.teacher.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    if (!subgroup) {
      throw new BadRequestException('no subgroup');
    }

    subgroup.tasks = (subgroup.tasks as Types.ObjectId[]).filter(
      (t) => t.toString() !== deleteTaskDto.task.toString(),
    );
    await subgroup.save();
    const deletedTask = await this.taskModel
      .findByIdAndDelete(deleteTaskDto.task)
      .exec();
    // await task.save();
    return deletedTask;
  }

  async updateGrade(
    updateSubgroupGradeDto: UpdateSubgroupGradeDto,
    user: UserDocument,
  ) {
    const course = await this.courseModel
      .findById(updateSubgroupGradeDto.course)
      .exec();

    const subgroup = await this.subgroupModel
      .findById(updateSubgroupGradeDto.subgroup)
      .exec();

    if (course.teacher.toString() !== user._id.toString()) {
      throw new ForbiddenException();
    }

    if (!subgroup) {
      throw new BadRequestException('no subgroup');
    }

    const grade = subgroup.grades.find((grade) => {
      return (
        grade.student.toString() === updateSubgroupGradeDto.student.toString()
      );
    });

    if (grade) {
      grade.grade = updateSubgroupGradeDto.grade;
    } else {
      subgroup.grades.push({
        student: Types.ObjectId(updateSubgroupGradeDto.student),
        grade: updateSubgroupGradeDto.grade,
      });
    }

    await subgroup.save();
  }

  // create(createSubgroupDto: CreateSubgroupDto) {
  //   return 'This action adds a new subgroup';
  // }

  // findAll() {
  //   return `This action returns all subgroups`;
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} subgroup`;
  // }

  // update(id: number, updateSubgroupDto: UpdateSubgroupDto) {
  //   return `This action updates a #${id} subgroup`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} subgroup`;
  // }
}

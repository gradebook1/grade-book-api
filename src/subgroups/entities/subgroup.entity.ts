import * as mongoose from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Course } from 'src/courses/entities/course.entity';
import { Grade, GradeSchema, Task } from 'src/tasks/entities/task.entity';
import { GradeCalculationType } from 'src/enums/grade-calculation.enum';
import { Module } from 'src/modules/entities/module.entity';

export type SubgroupDocument = Subgroup & Document;

@Schema()
export class Subgroup {
  @Prop({ required: true, trim: true })
  name: string;

  @Prop({ trim: true })
  description?: string;

  @Prop({ required: true })
  createdDate: string;

  @Prop({
    required: true,
    trim: true,
    enum: [GradeCalculationType.AVERAGE, GradeCalculationType.SUM],
  })
  gradeType: GradeCalculationType;

  // @Prop({
  //   required: true,
  //   type: mongoose.Schema.Types.ObjectId,
  //   ref: 'Course',
  // })
  // course: Course | mongoose.Types.ObjectId;

  // @Prop({ required: true, default: false })
  // isPrivate: boolean;

  // @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }] })
  // privateStudents: User[] | mongoose.Types.ObjectId[];

  // @Prop({ required: true, immutable: true })
  // isGroupOfModules: boolean;

  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Module' }] })
  modules: Module[] | mongoose.Types.ObjectId[];

  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Task' }] })
  tasks: Task[] | mongoose.Types.ObjectId[];

  @Prop({ type: [{ type: GradeSchema }] })
  grades: Grade[];
}

export const SubgroupSchema = SchemaFactory.createForClass(Subgroup);

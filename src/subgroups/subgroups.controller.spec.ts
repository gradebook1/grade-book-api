import { Test, TestingModule } from '@nestjs/testing';
import { SubgroupsController } from './subgroups.controller';
import { SubgroupsService } from './subgroups.service';

describe('SubgroupsController', () => {
  let controller: SubgroupsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SubgroupsController],
      providers: [SubgroupsService],
    }).compile();

    controller = module.get<SubgroupsController>(SubgroupsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

export enum GradeCalculationType {
  AVERAGE = 'average',
  SUM = 'sum',
}
